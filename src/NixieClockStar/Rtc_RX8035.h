/*
* Rtc_RX8035.h
* 
* �H���d�q RX-8035LC
* 
*/

#pragma once

#include "RTC.h"

class RX8035:public Rtc{
public:
	RX8035(uint8_t adrs = 0x32):Rtc(adrs){};
	uint8_t fetchCurrentTime(Calendar& cal);
	uint8_t setCurrentTime(const Calendar& cal);

private:
	/* RTC */
	uint8_t initRtc(void);
	
};












