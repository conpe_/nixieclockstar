/*
* StateMachine.h
* 
* 
* 
*/

#pragma once

#include "CommonDataType.h"
#include "Setting.h"

namespace Disp{
	
	int8_t init(void);
	int8_t run(void);
	
	/* 数値セット */
	int8_t setVal(const Digit dig[6]);
	int8_t setVal(const uint32_t dig, const bool blink = 0);		/* 全桁セット */
	
	/* コロン表示セット */
	int8_t setColon(const Digit& colon);
	int8_t setColon(const bool on, const bool blink = false);
	
	/* ニキシー管on/off(DCDC有効無効) */
	void on(void);
	void off(void);
	
	/* 表示明るさ */
	void setBrightness(uint8_t brightness);
	
	void setBlinkTime(uint16_t ontime, uint16_t offtime);
	void resetBlink(bool on = true);
	
	
	/* ダイナミック点灯処理 */
	void dinamic(void);
	
};

