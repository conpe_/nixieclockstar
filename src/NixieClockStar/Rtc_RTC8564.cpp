#include "Rtc_RTC8564.h"
#include "Setting.h"

uint8_t RTC8564::fetchCurrentTime(Calendar& cal){
	std::vector<uint8_t> rcvdata;
	
	if(0 == readReg(0x02, 7, &rcvdata)){
		
		cal.sec = decBcd(rcvdata[0]&0x7F);
		cal.minute = decBcd(rcvdata[1]&0x7F);
		cal.hour = decBcd(rcvdata[2]&0x3F);
		cal.day = decBcd(rcvdata[3]&0x3F);
		cal.weekday = decBcd(rcvdata[4]);
		cal.month = decBcd(rcvdata[5]&0x1F);
		cal.year = 2000 + decBcd(rcvdata[6]&0xFF);
		
		log_i("time %04d/%02d/%02d %02d:%02d:%02d", cal.year, cal.month, cal.day, cal.hour, cal.minute, cal.sec);

		if(rcvdata[0]&0x80){
			log_i("invalid time");
			return cInvalidTime;
		}else{
			return cNoError;
		}
	}else{
		
		
		return cNotConnect;
	}
	
	
}

uint8_t RTC8564::setCurrentTime(const Calendar& cal){
	std::vector<uint8_t> setdata;
	
	log_i("settime %04d/%02d/%02d %02d:%02d:%02d", cal.year, cal.month, cal.day, cal.hour, cal.minute, cal.sec);

	setdata.push_back(encBcd(cal.sec)&0x7F);
	setdata.push_back(encBcd(cal.minute)&0x7F);
	setdata.push_back(encBcd(cal.hour)&0x3F);
	setdata.push_back(encBcd(cal.day)&0x3F);
	setdata.push_back(encBcd(cal.weekday));
	setdata.push_back(encBcd(cal.month)&0x1F);
	setdata.push_back(encBcd(cal.year%100));	/* 下2桁 */
	
	setReg(0x00, 0x20);		/* stop */
	setReg(0x02, &setdata);
	setReg(0x00, 0x00);		/* start */
	
	return 0;
}


uint8_t RTC8564::initRtc(void){
	
	setReg(0x00, 0x00);		/* Control1レジスタ初期化 */
	setReg(0x01, 0x00);		/* Control2レジスタ初期化 */
	setReg(0x09, 0x80);		/* アラーム無効 */
	setReg(0x0A, 0x80);		/* アラーム無効 */
	setReg(0x0B, 0x80);		/* アラーム無効 */
	setReg(0x0C, 0x80);		/* アラーム無効 */
	setReg(0x0D, 0x00);		/* CLKOUT無効 */
	setReg(0x0E, 0x00);		/* タイマ割り込み無効 */


	/* 1秒周期出力 */
	setReg(0x01, 0x11);		/* TITP:繰り返し割り込みモード, TIE:INT出力有効 */
	setReg(0x0E, 0x02);		/* TD:1Hz */
	setReg(0x0F, 1);		/* timer初期値 */
	setReg(0x0E, 0x82);		/* TE:タイマ有効化, TD:1Hz */

	

	Calendar cal;
	if(cInvalidTime == fetchCurrentTime(cal)){
		cal.sec = 0;
		cal.minute = 17;
		cal.hour = 0;
		cal.day = 21;
		cal.weekday = 0;
		cal.month = 4;
		cal.year = 2019;
		
		return cInvalidTime;
	}
	
	return cNoError;
}


