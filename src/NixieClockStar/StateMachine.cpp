



#include "StateMachine.h"
#include "NtcDrv.h"
/* 各状態 */
#include "StateDispCurrentTime.h"
#include "StateSetTime.h"

StateDispCurrentTime state_disp_current_time;
StateSetTime state_set_time;


int8_t StateMachine::init(void){
	
	CurrentState = &state_disp_current_time;
	CurrentState->entry();
	
	button_long[0] = false;
	button_long[1] = false;
	button_long[2] = false;

	return 0;
}

int8_t StateMachine::run(void){
	bool next = false;
	
	/* ボタン立ち上がり判定 */
	bool up_rise = NtcDrv::sw_up.isRise();
	bool mode_rise = NtcDrv::sw_mode.isRise();
	bool mode_long = NtcDrv::sw_mode.isLong();
	bool set_long = NtcDrv::sw_set.isLong();
	
	
	/* ボタン入力内容で状態遷移させる前に各状態の処理をさせたい */
	/* run()→状態遷移判定→exit()→enter() */
	
	
	/* 実行 */
	if(nullptr != CurrentState){
		CurrentState->run();
	}
	
	
	/* 状態遷移 */
	State* next_state = CurrentState;
	if(CurrentState == &state_disp_current_time){
		if(set_long && !button_long[1]){
			next_state = &state_set_time;	/* 設定画面へ */
			log_i("State = Setting.");
			next = true;
		}else if(mode_long && !button_long[0]){
			NtcDrv::ntp.beginSmartConfig();
			next_state = &state_disp_current_time;	/* 現在時刻表示 */
			log_i("State = Current time.");
			log_i("Start smartconfig...");
			next = true;
		}
	}else if(CurrentState == &state_set_time){
		if(mode_rise || (set_long && !button_long[1])){
			next_state = &state_disp_current_time;	/* 現在時刻表示へ */
			next = true;
			log_i("State = Current time.");
		}
	}
	
	if(next){
		if(nullptr != CurrentState){
			CurrentState->exit();
		}
		if(nullptr != next_state){
			next_state->entry();
			CurrentState = next_state;
		}
	}
	
	
	button_long[0] = mode_long;
	button_long[1] = set_long;

	return 0;
}




