/*
* StateMachine.h
* 
* 
* 
*/

#pragma once

#include "State.h"

class StateDispCurrentTime : public State{
public:
	StateDispCurrentTime(void);
	
	int8_t entry(void);
	int8_t run(void);
	int8_t exit(void);
	
	void startRandomFlash(void);
	
private:
	const uint32_t cUpdateDispFrqCnt = 5;		/* 表示更新間隔( * 10ms周期run ) */
	uint32_t update_disp_cnt;
	/* 表示切り替え後自動で時分秒表示に戻る時間 */
	uint32_t return_normal_disp_cnt;
	

	enum eDispMode{
		eDispNone = 0,
		eDispHourMinuteSec = 1,	/* 時分秒表示 */
		eDispYearMonthDay = 2,	/* 年月日表示 */
		eDispNum,
	};
	
	eDispMode disp_mode;
	
	Digit colon;

	/* ランダムフラッシュ表示 */
	bool IsRamdomFlash;
	const uint16_t cRandomFlashTimeCnt = 300;	/* ランダム表示全桁確定までの時間 */
	const uint8_t cRandomFlashRandomCnt = 200;	/* 全桁ランダム表示する時間 */
												/* 2つを足して100(=1sec)以上にすること */
	uint16_t random_flash_time;
	
	
};


