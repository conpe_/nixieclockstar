/**************************************************
	DataMemList.cpp
	EEPROMメモリ管理
				2021/08/10 9:52:26
**************************************************/

#include "DataMemList.h"


/* EEPROM変数 */
stMem<uint8_t> mem_initialized = {1, 0, 1};		/* EEPROM初期化済みフラグ */
stMem<uint32_t> mem_ontime = {0, 0, 4294967295};		/* 積算通電時間 */
stMem<uint32_t> mem_ret_hms_time = {500, 0, 30000};		/* 時分秒復帰時間 */
stMem<uint8_t> mem_colontype_hms = {0, 0, 2};		/* 時分秒表示時のコロン点灯選択 */
stMem<uint8_t> mem_colontype_ymd = {0, 0, 2};		/* 年月日表示時のコロン点灯選択 */


/* EEPROM管理テーブル */
const ST_MEM_TABLE_T stMemTable[MEM_ID_NUM] = {
	/*	Name[PARAM_NAME_MAXLEN],		DataId,		Adrs,		DataType,		InitVal,		MinVal,		MaxVal,		ExternalAccessPermission,		*/
	{	"", MEM_INITIALIZED,		0x0000,		MEM_UINT8,		&mem_initialized.Init,		&mem_initialized.Min,		&mem_initialized.Max,		DATAMEM_PERMISSION_READ_ONLY,		},		/* EEPROM初期化済みフラグ (Resolution=1, Offset=0) */
	{	"time_on_time", MEM_ONTIME,		0x0001,		MEM_UINT32,		&mem_ontime.Init,		&mem_ontime.Min,		&mem_ontime.Max,		DATAMEM_PERMISSION_READ_ONLY,		},		/* 積算通電時間 (Resolution=1, Offset=0) */
	{	"rettime_hms", MEM_RET_HMS_TIME,		0x0005,		MEM_UINT32,		&mem_ret_hms_time.Init,		&mem_ret_hms_time.Min,		&mem_ret_hms_time.Max,		DATAMEM_PERMISSION_FULL,		},		/* 時分秒復帰時間 (Resolution=1, Offset=0) */
	{	"colontype_hms", MEM_COLONTYPE_HMS,		0x0009,		MEM_UINT8,		&mem_colontype_hms.Init,		&mem_colontype_hms.Min,		&mem_colontype_hms.Max,		DATAMEM_PERMISSION_FULL,		},		/* 時分秒表示時のコロン点灯選択 (Resolution=1, Offset=0) */
	{	"colontype_ymd", MEM_COLONTYPE_YMD,		0x000A,		MEM_UINT8,		&mem_colontype_ymd.Init,		&mem_colontype_ymd.Min,		&mem_colontype_ymd.Max,		DATAMEM_PERMISSION_FULL,		},		/* 年月日表示時のコロン点灯選択 (Resolution=1, Offset=0) */
};

/* ・Name無しは表示しない */
/* ・DataIdの上位1byteがグループID */

/* グループ管理テーブル */
const ST_MEM_GROUP_TABLE_T stMemGroupTable[MEM_GROUP_NUM] = {
	/*	Name[PARAM_NAME_MAXLEN]	*/
	{	"Util"	},		/* Group0 Util */
	{	"CurTime"	},		/* Group1 CurTime */
};
