/**************************************************
	DataMemList.h
	EEPROMメモリ管理
				2021/08/10 9:52:48
**************************************************/

#ifndef __DATAMEMLIST_H__
#define __DATAMEMLIST_H__

#include "stdint.h"
#include "DataMemType.h"

/* EEPROM容量 [byte] */
#define MEM_CAPACITY 64	/* 容量(byte) */

/* EEPROM使用量 [byte] */
#define MEM_USED_BYTE 11	/* 使用量(byte) */

/* EEPROM使用ID数 */
#define MEM_ID_NUM 5	/* 使用ID数 */

/* グループ数 */
#define MEM_GROUP_NUM 2	/* グループ数 */



/* EEPROM ID */
#define	MEM_INITIALIZED	0x0000	/* EEPROM初期化済みフラグ */
#define	MEM_ONTIME	0x0001	/* 積算通電時間 */
#define	MEM_RET_HMS_TIME	0x0100	/* 時分秒復帰時間 */
#define	MEM_COLONTYPE_HMS	0x0101	/* 時分秒表示時のコロン点灯選択 */
#define	MEM_COLONTYPE_YMD	0x0102	/* 年月日表示時のコロン点灯選択 */


/* EEPROM変数 */
extern stMem<uint8_t> mem_initialized;		/* EEPROM初期化済みフラグ */
extern stMem<uint32_t> mem_ontime;		/* 積算通電時間 */
extern stMem<uint32_t> mem_ret_hms_time;		/* 時分秒復帰時間 */
extern stMem<uint8_t> mem_colontype_hms;		/* 時分秒表示時のコロン点灯選択 */
extern stMem<uint8_t> mem_colontype_ymd;		/* 年月日表示時のコロン点灯選択 */


/* EEPROM管理テーブル */
extern const ST_MEM_TABLE_T stMemTable[MEM_ID_NUM];
/* グループ管理テーブル */
extern const ST_MEM_GROUP_TABLE_T stMemGroupTable[MEM_GROUP_NUM];


#endif

