/*******************************************************************************

*******************************************************************************/

#ifndef DRVMEM_ESP32FLASH_H
#define DRVMEM_ESP32FLASH_H

#include <string>
#include <vector>
#include <stdint.h>
#include "drvMem.h"

#define MEM_FILENAME_DEFAULT "/DRV_MEM_DATA.bin"
#define MEM_SIZE_DEFAULT 128

class DrvMemEsp32Flash:public DrvMem{
public:
    DrvMemEsp32Flash(std::string fname = MEM_FILENAME_DEFAULT, uint16_t memsize=MEM_SIZE_DEFAULT);
    virtual int begin(void);
    virtual int begin(uint16_t memsize=MEM_SIZE_DEFAULT);
    virtual int close(void);
    virtual int read(uint16_t adrs, uint8_t* data, uint8_t len);
    virtual int write(uint16_t adrs, uint8_t* data, uint8_t len);
private:
    std::string filename;
    std::vector<uint8_t> mem;
};


#endif //DRVMEM_H
