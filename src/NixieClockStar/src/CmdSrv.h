/*******************************************************************************

*******************************************************************************/

#ifndef CMDSRV_H
#define CMDSRV_H

#include <stdint.h>

#define CMDSRV_DATA_LEN_MAX         (13)		// 最大パケット長
#define CMDSRV_DF_LEN_MAX			(8)		// 最大データ長

class CmdSrv{
public:
    uint8_t notify(uint8_t* rcv_msg, uint8_t rcv_len);
    uint8_t send(uint8_t cmd_id, uint8_t* data, uint8_t len);    /* こちらから通信開始 */
private:

    uint8_t BufData[CMDSRV_DATA_LEN_MAX] = {};
	uint8_t BufDataIdx = 0;
	uint8_t BufDataSize = CMDSRV_DATA_LEN_MAX;
	uint8_t BufDataDlc = CMDSRV_DF_LEN_MAX;
    
    uint8_t sendPacket(uint16_t header, uint8_t cmd_id, uint8_t* data, uint8_t len);
    virtual uint8_t write(uint8_t* send_msg, uint8_t send_len) = 0;

    static uint8_t calcCheckSum(uint8_t* data, uint8_t len);
};

#endif //CMDSRV_H
