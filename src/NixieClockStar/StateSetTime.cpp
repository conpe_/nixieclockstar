

#include <Arduino.h>

#include "StateSetTime.h"
#include "NtcDrv.h"

StateSetTime::StateSetTime(void){
	set_mode = eSetYear;
	cnt_up_longpush = 0;
}


int8_t StateSetTime::entry(void){

	Disp::setColon(0, false);	/* コロン表示なし */
	Disp::setBlinkTime((blink_freq_time_ms_set*blink_duty_set/100) * 1000 / dinamic_freq_time_us, (blink_freq_time_ms_set*(100-blink_duty_set)/100) * 1000 / dinamic_freq_time_us);
	Disp::resetBlink();
	
	
	set_mode = eSetYear;
	
	/* 変更時刻に現在時刻セット */
	NtcDrv::getTime(&setcal);
	log_i("current time %04d/%02d/%02d %02d:%02d:%02d", setcal.year, setcal.month, setcal.day, setcal.hour, setcal.minute, setcal.sec);
	setcal.sec = 0;
		
	cnt_up_longpush = 0;
	
	return 0;
}

int8_t StateSetTime::run(void){
	
	/* ボタン立ち上がり判定 */
	bool up_rise = NtcDrv::sw_up.isRise();
	bool set_rise = NtcDrv::sw_set.isRise();
	bool mode_rise = NtcDrv::sw_mode.isRise();
	/* ボタン長押し判定 */
	bool up_long_tmp = NtcDrv::sw_up.isLong();
	bool set_long = NtcDrv::sw_set.isLong();
	
	/* 値変更の長押しは3回に一度 */
	bool up_long = false;
	if(up_long_tmp){
		if(3<++cnt_up_longpush){
			up_long = true;
			cnt_up_longpush = 0;
		}
	}
	
	
	/* 押されたら点滅リセット */
	if(up_rise || set_rise || up_long){
		Disp::resetBlink();
	}
	
	/* 表示状態遷移 */
	if(mode_rise){
		/* セットせず戻る */
		
	}else if(set_long){

	}else{
		/* 値変更 */
		switch(set_mode){
		case eSetYear:
			if(up_rise || up_long){
				setcal.year++;
				if(2099<setcal.year){
					setcal.year = 2000;
				}
			}else if(set_rise){
				set_mode = eSetMonth;
			}
			break;
		case eSetMonth:
			if(up_rise || up_long){
				setcal.month++;
				if(12<setcal.month){
					setcal.month = 1;
				}
			}else if(set_rise){
				set_mode = eSetDay;
			}
			break;
		case eSetDay:
			if(up_rise || up_long){
				setcal.day++;
				if(Calendar::getDays(setcal.month, setcal.year) < setcal.day){
					setcal.day = 1;
				}
			}else if(set_rise){
				set_mode = eSetHour;
			}
			break;
		case eSetHour:
			if(up_rise || up_long){
				setcal.hour++;
				if(23<setcal.hour){
					setcal.hour = 0;
				}
			}else if(set_rise){
				set_mode = eSetMinute;
			}
			break;
		case eSetMinute:
			if(up_rise || up_long){
				setcal.minute++;
				if(59<setcal.minute){
					setcal.minute = 0;
				}
			}else if(set_rise){
				set_mode = eSetYear;
			}
			break;
		default:
			break;
		}
	}
	
	/* 月とかを弄って日数が変わった場合の処理 */
	if(Calendar::getDays(setcal.month, setcal.year) < setcal.day){
		setcal.day = Calendar::getDays(setcal.month, setcal.year) ;
	}
	
	/* 100msに一回表示更新 */
	++update_disp_cnt;
	if(update_disp_cnt >= cUpdateDispFrqCnt){
		
		/* 表示セット */
		Digit dig[digit_num];
		for(uint8_t i=0; i<digit_num; ++i){
			dig[i].on = true;
			dig[i].blink = false;
		}
		switch(set_mode){
		case eSetYear:
			
			dig[5].val = (setcal.year/1000) % 10;
			dig[4].val = (setcal.year/100) % 10;
			dig[3].val = (setcal.year/10) % 10;
			dig[2].val = setcal.year%10;
			
			dig[5].blink = true;
			dig[4].blink = true;
			dig[3].blink = true;
			dig[2].blink = true;
			
			dig[1].on = false;
			dig[0].on = false;
			
			break;
		case eSetMonth:
			
			dig[5].val = (setcal.month/10) % 10;
			dig[4].val = setcal.month % 10;
			dig[3].val = (setcal.day/10) % 10;
			dig[2].val = setcal.day%10;
			
			dig[5].blink = true;
			dig[4].blink = true;
			dig[3].blink = false;
			dig[2].blink = false;
			
			dig[1].on = false;
			dig[0].on = false;
			
			break;
		case eSetDay:
			
			dig[5].val = (setcal.month/10) % 10;
			dig[4].val = setcal.month % 10;
			dig[3].val = (setcal.day/10) % 10;
			dig[2].val = setcal.day%10;
			
			dig[5].blink = false;
			dig[4].blink = false;
			dig[3].blink = true;
			dig[2].blink = true;
			
			dig[1].on = false;
			dig[0].on = false;
			
			break;
		case eSetHour:
			
			dig[5].val = (setcal.hour/10) % 10;
			dig[4].val = setcal.hour % 10;
			dig[3].val = (setcal.minute/10) % 10;
			dig[2].val = setcal.minute%10;
			
			dig[5].blink = true;
			dig[4].blink = true;
			dig[3].blink = false;
			dig[2].blink = false;
			
			dig[1].on = false;
			dig[0].on = false;
			
			break;
		case eSetMinute:
			
			dig[5].val = (setcal.hour/10) % 10;
			dig[4].val = setcal.hour % 10;
			dig[3].val = (setcal.minute/10) % 10;
			dig[2].val = setcal.minute%10;
			
			dig[5].blink = false;
			dig[4].blink = false;
			dig[3].blink = true;
			dig[2].blink = true;
			
			dig[1].on = false;
			dig[0].on = false;
			
			break;
		default:
			
			dig[0].val = 0;
			dig[1].val = 0;
			
			dig[2].val = 0;
			dig[3].val = 0;
			
			dig[4].val = 0;
			dig[5].val = 0;
			
			break;
		}
		
		
		/* 表示 */
		Disp::setVal(dig);
		
	}
	
	
	return 0;
}

int8_t StateSetTime::exit(void){
	
	if(NtcDrv::sw_set.isLong()){	/* 長押しで抜けるなら時刻を確定させる */
		/* セットして戻る */
		NtcDrv::setTime(&setcal);
		offStatusLed();
		
	}else{
		log_i("no set");
	}

	return 0;
}






