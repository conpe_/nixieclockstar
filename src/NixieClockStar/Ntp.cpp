

#include "Ntp.h"
#include <WiFi.h>
#include <time.h>

#include "Setting.h"

#define JST     3600*9


int8_t Ntp::init(void){
	initStatusLed();

	Connected = false;
	state = 0;
	Req = 0x00;
	SmartConfigDone = false;
		
	return cNoError;
}


/* 10ms周期 */
void Ntp::update(void){

	/* 強制状態遷移 */
	if((0x03 == Req) && ((state & 0xF0)!=0x20)){	/* SmartConfig */
		state = 0;
	}

	switch(state){
	case 0x00:	
		/* idle */
		if(0x02 == Req){	/* 切断 */
			state = 0x30;
		}else if(0x01 == Req){	/* 接続 */
			if(!Connected){
				state = 0x10;
			}else{
				Req = 0x00;
			}
		}else if(0x03 == Req){
			state = 0x20;
		}
		break;
	case 0x10:	/* 接続開始 */
		WiFi.mode(WIFI_STA);
		WiFi.begin();	/* 前回値を使う */

		Connected = false;
		ConnectCnt = 0;
		state = 0x11;
	
		log_i("connecting...");
	
		break;
	case 0x11:
		if(WiFi.status() == WL_CONNECTED){
			log_i("Wifi connected. %s", WiFi.localIP().toString().c_str());
			log_i("conected SSID: %s", WiFi.SSID().c_str());
			Req = 0x00;
			state = 0x00;

			setNtpServer();

			Connected = true;

		}else{
			/* タイムアウトカウント */
			if(15*100 < ConnectCnt){	/* 15秒たってもつながらない */
				if(!SmartConfigDone){
					state = 0x20;		/* SmartConfigへ */
				}else{
					state = 0x30;		/* 終了 */
				}
			}else{
				++ConnectCnt;
			}

		}
		break;
	case 0x20:		/* SmartConfig */
		WiFi.mode(WIFI_AP_STA);			/* アクセスポイントモードで起動 */
		WiFi.beginSmartConfig();		/* SmartConfig開始 */
		log_i("start SmartConfig...");
		Connected = false;
		state = 0x21;
		break;
	case 0x21:
		if(WiFi.smartConfigDone()){		/* 完了 */
			ConnectCnt = 0;
			state = 0x22;
			offStatusLed();
		}else{
			if((++ConnectCnt / 50) & 0x01){	/* 500msごとにトグル */
				onStatusLed();
			}else{
				offStatusLed();
			}
#define SMARTCONFIG_TIMEOUT (100*300)
			if(ConnectCnt > SMARTCONFIG_TIMEOUT){
				/* タイムアウト */
				state = 0x22;
				offStatusLed();
				log_i("timeout");
			}else{
				/* 10秒ごとにタイムアウトまでの時間を表示 */
				if(0==(ConnectCnt%1000)){
					log_i("timeout for %dsec", (SMARTCONFIG_TIMEOUT - ConnectCnt)/100);
				}
			}
		}
		break;
	case 0x22:
		if(200 < ConnectCnt){
			if(0x03 == Req){
				Req = 0x00;
			}
			log_i("done SmartConfig...");
			WiFi.stopSmartConfig();

			SmartConfigDone = true;
			state = 0x00;
		}else{
			++ConnectCnt;
		}
		break;
	case 0x30:
		
		log_i("Wifi disconnect.");
		
		WiFi.disconnect(true);
		WiFi.mode(WIFI_OFF);

		Connected = false;
		Req = 0x00;
		state = 0x00;

		break;

	default:
		break;
	}


}

int8_t Ntp::setNtpServer(void){
	
	log_i("connect to ntp.nict.jp, ntp.jst.mfeed.ad.jp...");
	configTime( JST, 0, "ntp.nict.jp", "ntp.jst.mfeed.ad.jp");
	
	return cNoError;
}

int8_t Ntp::connect(void){
	Req = 0x01;
	
	return cNoError;
}

int8_t Ntp::disconnect(void){	/* 切断 */
	Req = 0x02;

	return cNoError;
}

int8_t Ntp::beginSmartConfig(void){
	Req = 0x03;
	SmartConfigDone = false;
	return cNoError;
}


int8_t Ntp::fetchCurrentTime(Calendar& cal){
	time_t t;
	struct tm *tm;
	//static const char *wd[7] = {"Sun","Mon","Tue","Wed","Thr","Fri","Sat"};
	
	if(isConnect()){
		
		t = time(NULL);
		tm = localtime(&t);
		
		if(tm->tm_year==70){	/* NTPサーバー接続判定(1970年だったらおかしい) */
			return cNotConnectNtpServer;
		}
		
		cal.year = tm->tm_year+1900;
		cal.month = tm->tm_mon+1;
		cal.weekday = tm->tm_wday;
		cal.day = tm->tm_mday;
		cal.hour = tm->tm_hour;
		cal.minute = tm->tm_min;
		cal.sec = tm->tm_sec;
		
		log_i("get time %04d/%02d/%02d %02d:%02d:%02d", cal.year, cal.month, cal.day, cal.hour, cal.minute, cal.sec);
		
		return cNoError;
	}else{
		return cNotConnectWifi;
	}
	
}

bool Ntp::isConnect(void){
	return Connected;
}

bool Ntp::isGotLocaltime(){
	time_t t = time(NULL);
	return t!=0;
}

