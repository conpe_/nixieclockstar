/*
* NtcDrv.h
* 
* ドライバ
* 
*/

#pragma once

#include <Wire.h>
#include "CommonDataType.h"
#include "Setting.h"
#include "Rtc.h"
#include "Ntp.h"
#include "Disp.h"

class Sw{
public:
	Sw(uint8_t port){
		this->port = port;
	}
	void init(void){
		initSw(port);
	}
	void update(void){
		is_push = readSw(port);
		
		/* 立ち上がり判定 */
		/* 制御周期ごとに判定 */
		is_rise = is_push && !is_push_last;
		is_push_last = is_push;
		
		/* 長押し判定 */
		if(is_push){
			if(long_cnt<long_cnt_th){
				long_cnt++;
			}
		}else{
			long_cnt = 0;
		}
	}
	bool isPush(void){
		return is_push;
	}
	bool isRise(void){
		
		return is_rise;
	}
	bool isLong(void){
		return long_cnt>=long_cnt_th;
	}
	void reset(void){
		long_cnt = 0;
		is_push_last = readSw(port);
		update();
	}
	
protected:
	uint8_t port;
	const uint32_t long_cnt_th = 100;	/* 長押しと判定するカウント */
	uint32_t long_cnt;
	bool is_push;
	bool is_rise;
	bool is_push_last;
};


class NtcDrv{
public:
	NtcDrv(void);
	static int8_t init(void);
	static int8_t run(void);
	
	static Rtc* rtc;
	static Ntp ntp;
	
	static Sw sw_mode;
	static Sw sw_set;
	static Sw sw_up;
	
	static void tick(void){ tick_flg = true; };		/* 1秒更新 */
	static void getTime(Calendar* gettime);
	static void setTime(Calendar* settime);

protected:
	static Calendar cal_local;			/* ローカル更新用 */
	/* NTP接続 */
	static uint8_t ntp_sv_state;		/*  */
	static const uint16_t cNtpStopTimeCnt;	/* NTP接続をあきらめる時間 */
	/* 表示更新カウント */
	static uint32_t nextupdatetime_rtc;
	/*  */
	static bool tick_flg;
};



