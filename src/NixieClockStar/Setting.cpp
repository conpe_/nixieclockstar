/*
* Setting.cpp
* 
* 
* 
*/

#include "Setting.h"

const uint8_t digit_num = 6;

const uint16_t dinamic_freq_time_us = 300;		/* ダイナミック点灯の周期 */
const uint16_t blink_freq_time_ms = 1000;		/* 現在時刻画面での点滅周期 */
const uint8_t blink_duty = 30;					/* 現在時刻画面での点滅デューティ */
const uint16_t blink_freq_time_ms_set = 500;	/* 設定画面での点滅周期 */
const uint8_t blink_duty_set = 50;				/* 設定画面での点滅デューティ */



/* タイマ割り込み設定 */
extern void tim_isr(void);

hw_timer_t* timer = nullptr;

void enableIsr(void){
	
	/* タイマ割り込み設定 */
	timer = timerBegin(1, 80, true);  /* 80MHz/80 */
	timerAttachInterrupt(timer, &tim_isr, true);
	timerAlarmWrite(timer, dinamic_freq_time_us, true);
	timerAlarmEnable(timer);
	
}

void disableIsr(void){
	timerAlarmDisable(timer);
}

