#include "Rtc_RX8035.h"
#include "Setting.h"

uint8_t RX8035::fetchCurrentTime(Calendar& cal){
	std::vector<uint8_t> rcvdata(0x00,7);
	
	if(0 == readReg(0x0F<<4, 1, &rcvdata)){
		if(
			(0!=(rcvdata[0]>>4 & 0x01))		/* PON */
			|| (0!=(rcvdata[0]>>5 & 0x01))	/* XST */
			//|| (0!=(rcvdata[0]>>6 & 0x01))	/* VDET */
		){
			log_i("RTC sta 0x%02x VDET%x,XSTP%x,PON%x,EDFG%x,CTFG%x,WkAFG%x,MoAFG%x", rcvdata[0], (rcvdata[0]>>6)&0x01,(rcvdata[0]>>5)&0x01,(rcvdata[0]>>4)&0x01,(rcvdata[0]>>3)&0x01,(rcvdata[0]>>2)&0x01,(rcvdata[0]>>1)&0x01,(rcvdata[0]>>0)&0x01);
			return cInvalidTime;
		}
	}else{
		return cNotConnect;
	}

	if(0 == readReg(0x00<<4, 7, &rcvdata)){
		
		cal.sec = decBcd(rcvdata[0]&0x7F);
		cal.minute = decBcd(rcvdata[1]&0x7F);
		if(0==rcvdata[2]){	/* 12時間制 */
			cal.hour = decBcd(rcvdata[2]&0x1F);
		}else{				/* 24時間制 */
			cal.hour = decBcd(rcvdata[2]&0x3F);
		}
		cal.weekday = decBcd(rcvdata[3]);
		cal.day = decBcd(rcvdata[4]&0x3F);
		cal.month = decBcd(rcvdata[5]&0x1F);
		cal.year = 2000 + decBcd(rcvdata[6]&0xFF);

		log_i("RTC: gettime %04d/%02d/%02d %02d:%02d:%02d", cal.year, cal.month, cal.day, cal.hour, cal.minute, cal.sec);
		
		if(rcvdata[0]&0x80){
			return cInvalidTime;
		}else{
			return cNoError;
		}
	}else{
		return cNotConnect;
	}
	
	
}

uint8_t RX8035::setCurrentTime(const Calendar& cal){
	std::vector<uint8_t> setdata;
	
	log_i("settime %04d/%02d/%02d %02d:%02d:%02d", cal.year, cal.month, cal.day, cal.hour, cal.minute, cal.sec);

	setReg(0x0F<<4, 0x00);			/* フラグクリア(bank0指定) */

	setdata.push_back(encBcd(cal.sec)&0x7F);
	setdata.push_back(encBcd(cal.minute)&0x7F);
	setdata.push_back((encBcd(cal.hour)&0x3F) | 0x80);	/* 24時間表示 */
	setdata.push_back(encBcd(cal.weekday));
	setdata.push_back(encBcd(cal.day)&0x3F);
	setdata.push_back(encBcd(cal.month)&0x1F);
	setdata.push_back(encBcd(cal.year%100));	/* 下2桁 */
	
	setReg(0x00<<4, &setdata);
	setReg(0x0F<<4, 0x00);			/* フラグクリア */
	
	return 0;
}


uint8_t RX8035::initRtc(void){

	/* 1sec周期出力 */
	setReg(0x0E<<4, 0x03);

	Calendar cal;
	if(cNoError != fetchCurrentTime(cal)){
		
		return cInvalidTime;
	}
	
	return cNoError;
}


