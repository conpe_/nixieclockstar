




#include "Disp.h"

#include "Setting.h"


namespace Disp{

	
	
	uint8_t current_digit;		/* 選択桁 */
	uint8_t dynamic_state = 0;		/* 制御周期の状態 */
	
	

	Digit* digit;
	Digit colon;
	
	uint8_t brightness;
	uint8_t bright_step_num = 4;	/* 明るさ調整段階(消灯〜MAX) */
	
	uint16_t blink_cnt;
	bool blink_current_on;
	
	uint16_t blink_cnt_ontime = 1000;
	uint16_t blink_cnt_offtime = 1000;

};


int8_t Disp::init(void){
	
	/* ポート初期化 */
	initColonPort();
	initBdcPort();
	initSelDigPort();
	initDcdcEnPort();
	
	offColon();
	outBDC(OUT_BDC_NONE);	/* none */
	outDig(OUT_DIG_NONE);
	disableDcdc();
	
	
	/* 表示情報 */
	digit = new Digit[digit_num];
	
	
	for(int i=0; i<digit_num; ++i){
		digit[i].val = 0;
		digit[i].on = false;
		digit[i].blink = false;
	}
	
	colon.val = 1;
	colon.on = true;
	colon.blink = true;
	
	/* 表示初期値 */
	current_digit = 0;
	
	setBrightness(bright_step_num - 1);	/* 初期値は明るさMAX */
	
	setBlinkTime((blink_freq_time_ms*blink_duty/100) * 1000 / dinamic_freq_time_us, (blink_freq_time_ms*(100-blink_duty)/100) * 1000 / dinamic_freq_time_us);
	resetBlink();
	
	return 0;
}


int8_t Disp::run(void){
	
	
	return 0;
}


/* 数値セット */
int8_t Disp::setVal(const Digit dig[6]){
	
	for(uint8_t i=0; i<digit_num; ++i){
		digit[i] = dig[i];
	}
	
	return 0;
}

int8_t Disp::setVal(const uint32_t dig, const bool blink){		/* 全桁セット */
	Digit digtmp[digit_num];
	uint32_t digval_tmp = dig;
	
	for(uint8_t i=0; i<digit_num; ++i){
		digtmp[i].val = digval_tmp%10;
		digtmp[i].on = true;
		digtmp[i].blink = blink;
		digval_tmp /= 10;
	}
	
	return setVal(digtmp);
}

void Disp::on(void){
	enableDcdc();
};
void Disp::off(void){
	disableDcdc();
};


/* コロン表示セット */
int8_t Disp::setColon(const Digit& colon){
	
	Disp::colon = colon;
	
	return 0;
}

int8_t Disp::setColon(const bool on, const bool blink){
	Digit colon;
	
	colon.val = on;
	colon.on = true;
	colon.blink = blink;
	setColon(colon);
	
	return 0;
}

/* 1〜4 */
void Disp::setBrightness(uint8_t brightness){
	//if(brightness >= bright_step_num){
	//	brightness = bright_step_num - 1;
	//}
	Disp::brightness = brightness;
}
	
void Disp::setBlinkTime(uint16_t ontime, uint16_t offtime){
	blink_cnt_ontime = ontime;
	blink_cnt_offtime = offtime;
};
void Disp::resetBlink(bool on){
	blink_cnt = 0;
	blink_current_on = on;
};




/* ダイナミック点灯&点滅処理 */
/* 周期的に呼ぶ */
void Disp::dinamic(void){
	
	/* 点滅制御 */
	++blink_cnt;
	if( blink_current_on ){		/* 点灯中 */
		if(blink_cnt_ontime <= blink_cnt){
			blink_cnt = 0;
			blink_current_on = false;	/* 消灯にする */
		}else{
			/* 点灯のまま */
		}
	}else{		/* 消灯中 */
		if(blink_cnt_offtime <= blink_cnt){
			blink_cnt = 0;
			blink_current_on = true;	/* 点灯にする */
		}else{
			/* 消灯のまま */
		}
	}
	
	/* 表示処理 */
	/* 処理周期：bright_step_num+1(ゴースト防止の消灯) */
	bright_step_num = 4;
	if(dynamic_state < bright_step_num){
		++dynamic_state;
	}else{
		dynamic_state = 0;
		
		/* 次の桁へ */
		++current_digit;
		if(digit_num <= current_digit){
			current_digit = 0;
		}
	}

	static uint16_t tim[6] = {0};
	static uint8_t current_rate_cnt[6] = {0};
	static uint8_t dig_last[6] = {0};
	static uint8_t disp_val_last[6] = {0};		/* これまで表示していた値 */

	/* 数字出力 */
	/* 数値変化時にじんわり移る */
	uint8_t val;
	uint8_t rate;
	const uint8_t rate_num = 5;			/* 濃淡の段階 最大5 */
	uint16_t switch_time;				/* 変化にかける時間 */

	if(brightness < 3){
		switch_time = 0;
	}else{
		switch_time = 100;	/* 変化にかける時間 */
	}

	if(digit[current_digit].val != dig_last[current_digit]){	/* 数値変わった */
		tim[current_digit] = switch_time;
		disp_val_last[current_digit] = dig_last[current_digit];
		current_rate_cnt[current_digit] = 0;
	}
	if(0 < tim[current_digit]){
		--tim[current_digit];
	}

	rate = (float)rate_num * (tim[current_digit]/((float)switch_time+1.0f));		/* 0~reta_num-1 */

	if(current_rate_cnt[current_digit] < (rate_num-1)){
		++current_rate_cnt[current_digit];
	}else{
		current_rate_cnt[current_digit] = 0;
	}
	// current_rate_cnt : 0~(rate_num-1)
	if(current_rate_cnt[current_digit] < rate){
		val = disp_val_last[current_digit];		/* 前に表示していた数値を表示する */
	}else{
		val = digit[current_digit].val;
	}

	dig_last[current_digit] = digit[current_digit].val;
	
	
	/* 点滅制御点灯中 かつ 該当桁がon かつ 明るさ設定内 */
	if((blink_current_on || (!digit[current_digit].blink)) && digit[current_digit].on && (dynamic_state < brightness)){
		/* 点灯 */
		outBDC(val);
		/* 桁選択 */
		outDig(current_digit);
	}else{
		/* 消灯 */
		/* 桁選択なし */
		outDig(OUT_DIG_NONE);
		/* 数字出力なし */
		//outBDC(OUT_BDC_NONE);	/* ないほうがきれい */
	}
	

	
	/* コロン出力 */
	if((blink_current_on || (!colon.blink)) && colon.on && (0!=colon.val) && (dynamic_state < (brightness + 1))){	/* 点灯中 かつ コロンがon かつ コロン点灯 */
		/* 点灯 */
		onColon();
	}else{
		/* 消灯 */
		offColon();
	}

}

