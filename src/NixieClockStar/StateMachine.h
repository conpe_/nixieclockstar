/*
* StateMachine.h
* 
* 
* 
*/

#pragma once

#include "State.h"
#include "CommonDataType.h"

class StateMachine{
public:
	int8_t init(void);
	int8_t run(void);		/* 10msごとに実行する */
	
private:
	State* CurrentState;
	bool button_long[3];
};












