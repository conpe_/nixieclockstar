/*
* StateMachine.h
* 
* 
* 
*/

#pragma once

#include "State.h"
#include "Ntp.h"

class StateSetTime : public State{
public:
	StateSetTime(void);
	
	int8_t entry(void);
	int8_t run(void);
	int8_t exit(void);
	
	
private:
	Calendar setcal;
	
	const uint32_t cUpdateDispFrqCnt = 10;		/* 表示更新間隔( * 10ms周期run ) */
	uint32_t update_disp_cnt;
	
	uint8_t cnt_up_longpush;
	
	enum eSetMode{
		eSetNone,
		eSetYear,	/* 年 */
		eSetMonth,	/* 月 */
		eSetDay,	/* 日 */
		eSetHour,	/* 分 */
		eSetMinute,	/* 分 */
		eSetSecond,	/* 秒 */
		eSetNum,
	};
	
	eSetMode set_mode;
	
};


