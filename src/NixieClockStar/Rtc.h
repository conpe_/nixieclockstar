/*
* Rtc.h
* 
* IICのRTC
* 
*/

#pragma once

#include <vector>
#include "CommonDataType.h"

class Rtc{
public:
	Rtc(uint8_t adrs);
	uint8_t init(void);
	virtual uint8_t fetchCurrentTime(Calendar& cal) = 0;
	virtual uint8_t setCurrentTime(const Calendar& cal) = 0;

public:
	static const uint8_t cNoError = 0;
	static const uint8_t cNotConnect = 1;
	static const uint8_t cInvalidTime = 2;
	
protected:
	uint8_t adrs;
	
	
	/* RTC */
	virtual uint8_t initRtc(void) = 0;
	
	/* I2C */
	uint8_t setReg(uint8_t reg, std::vector<uint8_t>* data);	/* 書き込み */
	uint8_t setReg(uint8_t reg, uint8_t data);		/* 1バイト書き込み */
	uint8_t setReg(int reg, int data);		/* 1バイト書き込み */
	uint8_t readReg(uint8_t reg, uint8_t num, std::vector<uint8_t>* data);
	
	static uint8_t decBcd(uint8_t bcd);
	static uint8_t encBcd(uint8_t bcd);
	
};












