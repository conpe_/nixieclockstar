/*
* Rtc_8564.h
* 
* �H���d�q RTC-8564NB
* 
*/

#pragma once

#include "RTC.h"

class RTC8564:public Rtc{
public:
	RTC8564(uint8_t adrs = 0x51):Rtc(adrs){};
	uint8_t fetchCurrentTime(Calendar& cal);
	uint8_t setCurrentTime(const Calendar& cal);

private:
	/* RTC */
	uint8_t initRtc(void);
	
};












