/*
* StateMachine.h
* 
* 
* 
*/

#pragma once

#include "CommonDataType.h"

class State{
public:
	virtual int8_t entry(void) = 0;
	virtual int8_t run(void) = 0;				/* 10msごとに実行する */
	virtual int8_t exit(void) = 0;
	
protected:
	
};





