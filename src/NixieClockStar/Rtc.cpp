
#include "Rtc.h"
#include "Setting.h"
#include "NtcDrv.h"

Rtc::Rtc(uint8_t adrs){
	this->adrs = adrs;
}


uint8_t Rtc::init(void){
	initRtcIntPin();
	
	return initRtc();
}


uint8_t Rtc::setReg(uint8_t reg, std::vector<uint8_t>* data){
	
	Wire.beginTransmission(Rtc::adrs);
	Wire.write(reg);
	
	for(auto itr = data->begin(); itr!=data->end(); ++itr){
		Wire.write(*itr);
	}
	
	Wire.endTransmission();
	
	return 0;
}

uint8_t Rtc::setReg(uint8_t reg, uint8_t data){
	std::vector<uint8_t> setdata;

	setdata.push_back(data);
	setReg(reg, &setdata);
	
	return 0;
}
uint8_t Rtc::setReg(int reg, int data){
	std::vector<uint8_t> setdata;

	setdata.push_back((uint8_t)data);
	setReg(reg, &setdata);
	
	return 0;
}

uint8_t Rtc::readReg(uint8_t reg, uint8_t num, std::vector<uint8_t>* data){
	
	data->clear();
	data->reserve(num);	/* 領域を確保 */
	
	Wire.beginTransmission(Rtc::adrs);
	Wire.write(reg);
	Wire.endTransmission(false);
	Wire.requestFrom(Rtc::adrs, num);
	for(int i=0; i<num; ++i){
		uint8_t read_data_tmp;
		if(!Wire.available()){	/* 受信できない */
		//	return 1;
		}
		read_data_tmp = Wire.read();
		data->push_back(read_data_tmp);
	}
	
	return 0;
}


uint8_t Rtc::decBcd(uint8_t bcd){
	return (bcd&0x0f) + ((bcd>>4)*10);
}
uint8_t Rtc::encBcd(uint8_t val){
	return (((val/10)%10)<<4 | (val%10));
}

