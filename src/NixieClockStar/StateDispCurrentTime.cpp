


#include "NtcDrv.h"
#include "src/DataMem.h"
#include "StateDispCurrentTime.h"



StateDispCurrentTime::StateDispCurrentTime(void){
	disp_mode = eDispHourMinuteSec;
	IsRamdomFlash = false;
}


int8_t StateDispCurrentTime::entry(void){
	colon.val = 1;
	colon.on = true;
	colon.blink = true;
	Disp::setColon(colon);
	Disp::setBlinkTime((blink_freq_time_ms*blink_duty/100) * 1000 / dinamic_freq_time_us, (blink_freq_time_ms*(100-blink_duty)/100) * 1000 / dinamic_freq_time_us);
	Disp::resetBlink();
	
	disp_mode = eDispHourMinuteSec;
	
	return_normal_disp_cnt = 0;
		

	return 0;
}

int8_t StateDispCurrentTime::run(void){
	static Calendar cal;
	
	/* ボタン立ち上がり判定 */
	bool up_rise = NtcDrv::sw_up.isRise();
	

	/* 表示状態遷移 */
	switch(disp_mode){
	case eDispHourMinuteSec:
		if(up_rise){		/* UPボタン立ち上がり */
			disp_mode = eDispYearMonthDay;
		}
		break;
	case eDispYearMonthDay:
		if(up_rise){		/* UPボタン立ち上がり */
			disp_mode = eDispHourMinuteSec;
		}
		break;
	default:
		break;
	}
	
	/* 無操作で時分秒表示へ戻る */
	if(up_rise){	/* UPボタン押下時にカウンタリセット */
		DataMem::read(MEM_RET_HMS_TIME, &return_normal_disp_cnt);
	}
	if(0<return_normal_disp_cnt){
		
		--return_normal_disp_cnt;
		
		if(0==return_normal_disp_cnt){
			disp_mode = eDispHourMinuteSec;
		}
	}
	
	
	/* ランダム表示処理 */
	static uint8_t rand_val = 0;
	if(IsRamdomFlash){
		rand_val += 2;
		if(rand_val > 99){
			rand_val = 0;
		}
		
		--random_flash_time;
		if(random_flash_time<=0){
			IsRamdomFlash = false;
		}
	}
	
	
	
	/* 100msに一回表示更新 */
	++update_disp_cnt;
	if(update_disp_cnt >= cUpdateDispFrqCnt){
		update_disp_cnt = 0;
		

		NtcDrv::getTime(&cal);

		
		/* フラッシュ表示トリガ */
		/* 15分毎に発動する */
		if(((cal.minute == 59)||(cal.minute == 14)||(cal.minute == 29)||(cal.minute == 44)) && (cal.sec==60-((cRandomFlashTimeCnt+cRandomFlashRandomCnt)/100)) && (false == IsRamdomFlash) ){
			startRandomFlash();
		}
		
		
		
		/* 表示セット */
		Digit dig[digit_num];
		for(uint8_t i=0; i<digit_num; ++i){
			dig[i].on = true;
			dig[i].blink = false;
		}
		switch(disp_mode){
		case eDispHourMinuteSec:
			
			dig[5].val = cal.hour/10;
			dig[4].val = cal.hour%10;
			
			dig[3].val = cal.minute/10;
			dig[2].val = cal.minute%10;
			
			dig[1].val = cal.sec/10;
			dig[0].val = cal.sec%10;
			
			break;
		case eDispYearMonthDay:
			
			dig[5].val = (cal.year%100)/10;
			dig[4].val = (cal.year)%10;
			
			dig[3].val = cal.month/10;
			dig[2].val = cal.month%10;
			
			dig[1].val = cal.day/10;
			dig[0].val = cal.day%10;
			
			break;
		default:
			
			dig[5].val = 0;
			dig[4].val = 0;
			
			dig[3].val = 0;
			dig[2].val = 0;
			
			dig[1].val = 0;
			dig[0].val = 0;
			
			break;
		}
		
		if(IsRamdomFlash){
			Digit dig_rand[digit_num];
			
			/* 表示確定した桁 */
			/* 0:全て未確定，1:1桁目確定, n:n桁目確定 ・・・ */
			uint8_t fix_digit_num;
			if(cRandomFlashTimeCnt < random_flash_time){
				fix_digit_num = 0;
			}else{
				fix_digit_num = (cRandomFlashTimeCnt - random_flash_time) / (cRandomFlashTimeCnt/digit_num) + 1;
			}
			
			for(uint8_t i=0; i<digit_num; ++i){
				if(fix_digit_num <= i){
					/* 未確定桁 */
					dig_rand[i].val = (rand_val/10+i)%10;
				}else{
					/* 確定桁 */
					dig_rand[i].val = dig[i].val;
				}
				
				dig_rand[i].on = true;
				dig_rand[i].blink = false;
			}
			
			for(uint8_t i=0; i<digit_num; ++i){
				dig[i] = dig_rand[i];
			}
		}
		
		/* 表示 */
		Disp::setVal(dig);


		/* コロン表示方式 */
		uint8_t colon_type = 0;
		if(eDispYearMonthDay == disp_mode){
			DataMem::read(MEM_COLONTYPE_YMD, &colon_type);
		}else if(eDispHourMinuteSec == disp_mode){
			DataMem::read(MEM_COLONTYPE_HMS, &colon_type);
		}

		switch(colon_type){
		case 0:		/* 点滅 */
			colon.on = true;
			colon.blink = true;
			break;
		case 1:		/* 消灯 */
			colon.on = false;
			colon.blink = false;
			break;
		case 2:		/* 点灯 */
			colon.on = true;
			colon.blink = false;
			break;
		}
		Disp::setColon(colon);
	}
	
	return 0;
}

int8_t StateDispCurrentTime::exit(void){
	
	return 0;
}


void StateDispCurrentTime::startRandomFlash(void){
	disp_mode = eDispHourMinuteSec;
	IsRamdomFlash = true;
	random_flash_time = cRandomFlashTimeCnt + cRandomFlashRandomCnt;
}



