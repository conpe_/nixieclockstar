/*
* Setting.h
* 
* 
* 
*/

#pragma once

#include <Arduino.h>
#include "CommonDataType.h"

extern const uint8_t digit_num;
extern const uint8_t colon_num;

extern const uint16_t dinamic_freq_time_us;
extern const uint16_t blink_freq_time_ms;
extern const uint8_t blink_duty;
extern const uint16_t blink_freq_time_ms_set;
extern const uint8_t blink_duty_set;



/* DCDCポート出力 */
#define initDcdcEnPort() pinMode(27, OUTPUT)
#define enableDcdc() digitalWrite(27, 1)
#define disableDcdc() digitalWrite(27, 0)

/* スイッチ入力ポート */
#define SW0 0
#define SW1 36
#define SW2 39
#define initSw(x) pinMode(x, INPUT_PULLUP)
#define readSw(x) !digitalRead(x)
#define initSw0() pinMode(SW0, INPUT)
#define initSw1() pinMode(SW1, INPUT_PULLUP)
#define initSw2() pinMode(SW2, INPUT_PULLUP)
#define readSw0() readSw(SW0)
#define readSw1() readSw(SW1)
#define readSw2() readSw(SW2)

/* 明るさ検出ポート入力 */
#define getAmb() analogRead(34)



/* コロンのポート出力 */
#define initColonPort() pinMode(19, OUTPUT)
#define onColon() digitalWrite(19, 1)
#define offColon() digitalWrite(19, 0)

/* BDCポート出力 */
#define initBdcPort() pinMode(16, OUTPUT);pinMode(17, OUTPUT);pinMode(5, OUTPUT);pinMode(18, OUTPUT)
#define outBDC(val) digitalWrite(16, val&0x01);digitalWrite(17, (val>>1)&0x01);digitalWrite(5, (val>>2)&0x01);digitalWrite(18, (val>>3)&0x01)
#define OUT_BDC_NONE 0xFF

/* 桁選択ポート出力 */
#define initSelDigPort() pinMode(26, OUTPUT);pinMode(25, OUTPUT);pinMode(32, OUTPUT);pinMode(33, OUTPUT);pinMode(14, OUTPUT);pinMode(15, OUTPUT)
#define outDig(dig_bit) digitalWrite(26, dig_bit!=0);digitalWrite(25, dig_bit!=1);digitalWrite(32, dig_bit!=2);digitalWrite(33, dig_bit!=3);digitalWrite(14, dig_bit!=4);digitalWrite(15, dig_bit!=5)
#define OUT_DIG_NONE 0xFF

/* RTC関係ポート設定 */
#define PIN_RTC_INT_1SEC (4)
#if defined(NIXIE_V2)	/* ver2 */
#define initRtcIntPin() pinMode(PIN_RTC_INT_1SEC, INPUT)
#else
#define initRtcIntPin() pinMode(PIN_RTC_INT_1SEC, INPUT_PULLUP)
#endif

/* Status LED */
#define initStatusLed() pinMode(23, OUTPUT)
#define onStatusLed() digitalWrite(23, 1)
#define offStatusLed() digitalWrite(23, 0)

/* タイマ割り込み設定 */
void enableIsr(void);
void disableIsr(void);




