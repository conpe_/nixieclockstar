/*
* StateMachine.h
* 
* 
* 
*/

#pragma once

#include <string>
#include "CommonDataType.h"

class Ntp{
public:
	int8_t init(void);
	void update(void);

	int8_t fetchCurrentTime(Calendar& cal);
	bool isConnect(void);
	int8_t connect(void);		/* 接続 */
	int8_t disconnect(void);	/* 切断 */
	int8_t beginSmartConfig(void);	/* SmartConnect */
	bool doneSmartConfig(void){return SmartConfigDone;};

	int8_t setNtpServer(void);	/* NTPサーバー設定 ネットワークに接続完了してからセットする */
	/* セット直後だとまだ時間取得できないので，少し待ってからfetchCurrentTimeする */
	bool isGotLocaltime(void);	/* 時刻取得できた */
public:
	enum er{
		cNoError = 0,
		cNotConnectWifi = 1,
		cNotConnectNtpServer = 1,
	};
	
private:

	uint8_t state;
	uint8_t Req;		/* 0x00:要求なし, 0x01:接続, 0x02:切断, 0x03:WiFi設定(SmartConfig) */
	bool Connected;

	uint32_t ConnectCnt;

	bool SmartConfigDone;
};












