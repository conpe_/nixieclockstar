

#include "Setting.h"
#include "NtcDrv.h"
#include "StateMachine.h"
#include "Disp.h"

#include "src/CmdSrv.h"
#include "src/DataMem.h"
#include "src/drvMemEsp32Flash.h"
#include "src/SerialCmd.h"

#define DYNAMIC_DRIVE_US 400    /* ダイナミック点灯周期 */

StateMachine state_machine;
DrvMemEsp32Flash drv_mem_flash;

SemaphoreHandle_t xMutex = NULL;
TaskHandle_t taskHandle[5];
void DispDynamic(void *pvParameters);

void ambCtrl(void); /* 明るさ制御 */
void SubTask(void *pvParameters);

void setup() {
  Serial.begin(115200);
  /* シリアルコマンド初期化 */
  serial_cmd.init(false);
  
  DataMem::init(&drv_mem_flash);
  /* 初期化チェック */
  uint8_t inited = 0;
  DataMem::read(MEM_INITIALIZED, &inited);
  if(1!=inited){
    DataMem::clearAll();
  }

  NtcDrv::init();
  Disp::init();
  state_machine.init();


  /* 表示開始 */
  Disp::on();

  xMutex = xSemaphoreCreateMutex();
  if( xMutex != NULL ){
    /* ダイナミック点灯用タスク開始 */
    //xTaskCreatePinnedToCore(DispDynamic, "dispdynamic", 2000, NULL, 0, &taskHandle[0], 0);
    /* シリアルコマンドタスク開始 */
    xTaskCreatePinnedToCore(SubTask, "subtask", 2000, NULL, 0, &taskHandle[0], 0);
  }else{
    Serial.println("rtos mutex create error, stopped");
  }

  //attachInterrupt(PIN_RTC_INT_1SEC, NtcDrv::tick, FALLING);

}


void loop() {
  static bool init = false;
  uint32_t current_millis = millis();
  uint32_t current_micros = micros();
  static bool tick_10ms = 0;
  static uint32_t cnt_50ms = 0;
  BaseType_t xStatus;

  static uint32_t next_tick = 10;
  static uint32_t next_dynamic_us = DYNAMIC_DRIVE_US;

  bool tick_isr;
  static bool tick_isr_last = false;

  tick_isr = digitalRead(PIN_RTC_INT_1SEC);
  if(!tick_isr_last && tick_isr){
    NtcDrv::tick();
  }
  tick_isr_last = tick_isr;

  if(!init){
    init = true;
    xSemaphoreGive(xMutex);
  }

  if( (next_tick <= current_millis)
    && (
      (next_tick >= 10)
      || ((next_tick < 10) && (current_millis <= 10))
    )
  ){
    tick_10ms = true;
    next_tick = current_millis + 10;
  }else{
    tick_10ms = false;
  }


  if(tick_10ms) {  /* 10msごと */
    xStatus = xSemaphoreTake(xMutex, 500);
    if(xStatus == pdTRUE){
      serial_cmd.task();
      
      NtcDrv::run();
      Disp::run();
      state_machine.run();

    }

    ++cnt_50ms;

    xSemaphoreGive(xMutex);
  }

  if(cnt_50ms >= 50) {  /* 500msごと */
    cnt_50ms = 0;

    xStatus = xSemaphoreTake(xMutex, 500);
    if(xStatus == pdTRUE){

      /* 表示明るさ制御 */
      ambCtrl();

    }
    xSemaphoreGive(xMutex);
  }


  if( (next_dynamic_us <= current_micros)
    && (
      (next_dynamic_us >= DYNAMIC_DRIVE_US)
      || ((next_dynamic_us < DYNAMIC_DRIVE_US) && (current_micros <= DYNAMIC_DRIVE_US))
    )
  ){
    Disp::dinamic();

    next_dynamic_us += DYNAMIC_DRIVE_US;
  }

  //delay(5);
}


void DispDynamic(void *pvParameters){
  BaseType_t xStatus;
  

  xSemaphoreGive(xMutex);

  while(1){
    xStatus = xSemaphoreTake(xMutex, 500);
    if(xStatus == pdTRUE ){
      Disp::dinamic();
      
    }
    xSemaphoreGive(xMutex);
    
    delayMicroseconds(dinamic_freq_time_us);
  }
}



/* 明るさ制御 */
/* 周囲の明るさに応じて，ニキシー管の明るさを調整する */
void ambCtrl(void){
  const uint16_t amb_th2 = 800;  /* 明るさ1,2変化しきい値 */
  const uint16_t amb_th3 = 1100;  /* 明るさ2,3変化しきい値 */
  const uint16_t amb_th4 = 1500;  /* 明るさ3,4変化しきい値 */
  const uint16_t amb_hys = 50; /* 明るさ変化のヒス */
  
  static uint8_t current_disp_bright = 0;
  
  uint8_t disp_bright = current_disp_bright;
  uint16_t amb = getAmb(); /* 0(暗)～3000(明)くらい */

      
  /* 明るさを段階的に変える */
  /* ヒスあり */
  /* 上げ方向 */
  if(current_disp_bright<2 && (amb >= amb_th2 + amb_hys)){
    disp_bright = 2;
  }
  if(current_disp_bright<3 && (amb >= amb_th3 + amb_hys)){
    disp_bright = 3;
  }
  if(current_disp_bright<4 && (amb >= amb_th4 + amb_hys)){
    disp_bright = 4;
  }
  /* 下げ方向 */
  if(current_disp_bright>3 && (amb <= amb_th4 - amb_hys)){
    disp_bright = 3;
  }
  if(current_disp_bright>2 && (amb <= amb_th3 - amb_hys)){
    disp_bright = 2;
  }
  if(current_disp_bright>1 && (amb <= amb_th2 - amb_hys)){
    disp_bright = 1;
  }

  if(disp_bright != current_disp_bright){
    /* ニキシー管表示制御に明るさをセット */
    Disp::setBrightness(disp_bright);
    
    log_i("disp_brightness 　%d: %d -> %d", amb, current_disp_bright, disp_bright);

    /* 前回値保持 */
    current_disp_bright = disp_bright;

  }

}

void SubTask(void *pvParameters){
  static uint32_t sec_cnt_nextupdate_time = 1000;

  while(1){
    //serial_cmd.task();

    uint32_t current_millis = millis();

    if(
      (sec_cnt_nextupdate_time <= current_millis)
       && (
         (sec_cnt_nextupdate_time >= 1000)
        || ((sec_cnt_nextupdate_time < 1000) && (current_millis<=1000)) )  /* オーバーフロー時は現在時刻もオーバーフローしてから */
    ){
      if(1 == digitalRead(27)){   /* DCDC有効 = ニキシー管光ってる */
        uint32_t tmp;
        if(0 == DataMem::read(MEM_ONTIME, &tmp)){
          DataMem::write(MEM_ONTIME, ++tmp);  /* インクリメントして書き込み */

          if(0 == tmp%1800){ /* 定期的に保存 */
            DataMem::save();
          }
          if(0 == tmp%3600){
            log_i("on time = %d", tmp);
          }
        }
      }

      sec_cnt_nextupdate_time = current_millis + 1000;
    }

    vTaskDelay(20);
  }
}
