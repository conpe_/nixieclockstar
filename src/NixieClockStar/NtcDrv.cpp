

#include "NtcDrv.h"
#include "Setting.h"

#if defined(NIXIE_V2)		/* ver2 */
#include "Rtc_RX8035.h"
#else
#include "Rtc_RTC8564.h"
#endif

#define READ_RTC_FREQ (3610*1000)		/* RTCから時刻を読む周期[ms] */

Rtc* NtcDrv::rtc = nullptr;
Ntp NtcDrv::ntp;

Sw NtcDrv::sw_mode(SW0);
Sw NtcDrv::sw_set(SW1);
Sw NtcDrv::sw_up(SW2);

Calendar NtcDrv::cal_local;
uint32_t NtcDrv::nextupdatetime_rtc;
uint8_t NtcDrv::ntp_sv_state = 0;		/* 起動後すぐに接続スタート */
const uint16_t NtcDrv::cNtpStopTimeCnt = 300*100;
bool NtcDrv::tick_flg = false;

NtcDrv::NtcDrv(void){
	
}


int8_t NtcDrv::init(void){
	
	sw_mode.init();
	sw_set.init();
	sw_up.init();
	
	Wire.begin();


#if defined(NIXIE_V2)		/* ver2 */
	rtc = new RX8035();
#else
	rtc = new RTC8564();
#endif
	rtc->init();	/* RTC初期化 */

	ntp.init();


	/* 表示更新カウント */
	nextupdatetime_rtc = millis();

	return 0;
}

int8_t NtcDrv::run(void){
	
	sw_mode.update();
	sw_set.update();
	sw_up.update();

	ntp.update();




	/* ローカルタイム更新 */
	uint32_t current_millis = millis();

	if(tick_flg){
		//noInterrupts();
		tick_flg = false;
		//interrupts();
		
		++cal_local;
		
		/* ドットblink同期 */
		Disp::resetBlink();
	}



	Calendar cal;
	Calendar cal_tmp;
	bool is_get_ntp = false;
	bool is_get_rtc = false;
	static uint16_t ntp_sv_not_connect_cnt = 0;

	/* 接続したら読む */
	switch(ntp_sv_state){
	case 0:		/* NTPサーバー接続開始 */
		ntp.connect();		/* 接続要求 */
		ntp_sv_state = 1;
		ntp_sv_not_connect_cnt = 0;

		log_i("NTP start.");
		break;
	case 1:		/* 時刻取得 */
		/* 取得成功 */
		if(Ntp::cNoError == ntp.fetchCurrentTime(cal_tmp)){
			cal = cal_tmp;

			onStatusLed();
			
			is_get_ntp = true;
			
			/* RTCにセット */
			rtc->setCurrentTime(cal);
			
			
			ntp_sv_state = 2;
		}else{
			/* 指定時間たっても取得完了しなかったら諦める */
			if(ntp_sv_not_connect_cnt > (10*100)){		/* 10sec */
				ntp_sv_state = 2;
				log_i("Server timeout.");
			}else{
				ntp_sv_not_connect_cnt++;
			}
		}
		break;
	case 2:	/* 終了処理 */
		/* 接続解除 */
		ntp.disconnect();
		ntp_sv_state = 0xFF;
		break;
	default:
		break;
	}
	
	if(!is_get_ntp){	/* NTPから読まなかったら */
		/* RTCから読む */
		if( (nextupdatetime_rtc <= current_millis)
			&& ((nextupdatetime_rtc >= READ_RTC_FREQ )
				|| ((nextupdatetime_rtc < READ_RTC_FREQ) && (current_millis <= READ_RTC_FREQ )) )	/* オーバーフロー時 */
			){
			
			if(Rtc::cNoError == rtc->fetchCurrentTime(cal_tmp)){
				cal = cal_tmp;

				is_get_rtc = true;
			}else{
				log_i("RTC: read error");
			}

			nextupdatetime_rtc = current_millis + READ_RTC_FREQ;
		}
	}

	if(is_get_ntp || is_get_rtc){		/* 時刻取得できた */
		cal_local = cal;				/* ローカルタイムを同期 */
	}

		
	/* 3時間に一回NTPに接続しに行く */
	if( (cal_local.hour%3 == 0) && (cal_local.minute==14) && (cal_local.sec==30) && (0xFF==ntp_sv_state) ){
		/* 3で割り切れる時14分50秒になったとき */
		ntp_sv_state = 0;
	}
	/* SmartConfig終了時にNTPに接続しに行く */
	bool sc_done = ntp.doneSmartConfig();
	static bool sc_done_last = false;
	if(sc_done && !sc_done_last && (0xFF==ntp_sv_state)){
		ntp_sv_state = 0;
	}
	sc_done_last = sc_done;

	return 0;
}


void NtcDrv::getTime(Calendar* gettime){
	*gettime = cal_local;
}

void NtcDrv::setTime(Calendar* settime){
	cal_local = *settime;
	rtc->setCurrentTime(*settime);
}
