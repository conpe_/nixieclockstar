/*
* StateMachine.h
* 
* 
* 
*/

#pragma once

#include <stdint.h>
#include <time.h>

class Digit{
public:
	uint8_t val;
	bool on;
	bool blink;
};

class Calendar{
public:
	int16_t year;	/* 年 (2021 = 2021年) */
	int8_t month;	/* 月 (1 = 1月) */
	int8_t weekday;	/* 曜日 (0 = 日) */
	int8_t day;		/* 日 (1 = 1日) */
	int8_t hour;	/* 時 */
	int8_t minute;	/* 分 */
	int8_t sec;		/* 秒 */
	
	Calendar(void):year(2000),month(1),weekday(0),day(1),hour(0),minute(0),sec(0){};

	static uint8_t getDays(const uint8_t month, const uint8_t year=0){
		/* 日付範囲チェック */
		switch(month){
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			return 31;
			break;
		case 4:
		case 6:
		case 9:
		case 11:
			return 30;
			break;
		case 2:
			if(0==year%4){	/* 4で割り切れる年は原則うるう年 */
				if((0==year%100) && (0!=year%400)){		/* ただし，100で割り切れて400で割り切れない年はうるう年でない */
					return 28;
				}else{
					return 29;
				}
			}else{
				return 28;
			}
			break;
		default:
			return 0;
			break;
		}
		
	}
	
	/* 日〜秒を秒(unixtime)に直して返す */
	static uint32_t calcSec(const Calendar& cal){
		uint32_t s = 0;
		struct tm date;
		
		s += cal.day*60*60;
		s += cal.hour*60*60;
		s += cal.minute*60;
		s += cal.sec;

		date.tm_year = cal.year - 1900;
		date.tm_mon = cal.month - 1;
		date.tm_mday = cal.day;
		date.tm_hour = cal.hour;
		date.tm_min = cal.minute;
		date.tm_sec = cal.sec;
		date.tm_yday = 0;

		s = mktime(&date);
		
		return s;
	}
	static void calcTime(const uint32_t sec, Calendar& cal){
		uint32_t s = 0;
		struct tm date;
		
		localtime_r((time_t*)&sec, &date);

		s += cal.day*60*60;
		s += cal.hour*60*60;
		s += cal.minute*60;
		s += cal.sec;

		cal.year = date.tm_year + 1900;
		cal.month = date.tm_mon + 1;
		cal.day = date.tm_mday;
		cal.hour = date.tm_hour;
		cal.minute = date.tm_min;
		cal.sec = date.tm_sec;
		cal.weekday = date.tm_wday;
		
	}

	
/* 演算子 */
	/* 足し算(秒で返す) */
	uint32_t operator + (Calendar cal){
		return calcSec((Calendar &)*this) + calcSec(cal);
	}
	/* 引き算(秒で返す) */
	uint32_t operator - (Calendar cal){
		return calcSec((Calendar &)*this) - calcSec(cal);
	}
	/* インクリメント */
	Calendar& operator ++(){	/* pre */
		uint32_t sec = calcSec((Calendar &)*this);
		calcTime(++sec, (Calendar &)*this);

		return *this;
	}
	Calendar operator ++(int){	/* post */
		Calendar tmp = *this;
		uint32_t sec = calcSec((Calendar &)*this);
		calcTime(++sec, (Calendar &)*this);

		return tmp;
	}
	/* デクリメント */
	Calendar& operator --(){	/* pre */
		uint32_t sec = calcSec((Calendar &)*this);
		calcTime(--sec, (Calendar &)*this);

		return *this;
	}
	Calendar operator --(int){	/* post */
		Calendar tmp = *this;
		uint32_t sec = calcSec((Calendar &)*this);
		calcTime(--sec, (Calendar &)*this);

		return tmp;
	}

};









